class Modal {
    constructor(name, content) {
        this.name = name;
        this.content = content;
    }

    render() {
        this.modal = document.createElement('div');
        this.modal.className = 'my-modal';


        this.header = document.createElement('div');
        this.header.className = 'form-header';
        this.title = document.createElement('span');
        this.title.className = 'form-title';
        this.title.innerText = this.name;
        this.header.append(this.title);
        this.modal.append(this.header);


        this.close = document.createElement('span');
        this.close.className = 'form-close';
        this.close.innerText = 'x';
        this.header.append(this.close);

        this.contentForm = document.createElement('div');
        this.contentForm.className = 'form-content';
        this.contentForm.append(this.content);

        this.modal.append(this.contentForm);

        this.close.addEventListener('click', () => {
            this.modal.remove();
        });

        document.body.append(this.modal);
    }
}

class Input {
    constructor(type = '', className = '', id = '', name = '', value = '', placeholder = '', required = '') {
        this._type = type;
        this._className = className;
        this._id = id;
        this._name = name;
        this._value = value;
        this._placeholder = placeholder;
        this._required = required;
        this.elem = null;
    }

    render() {
        this.elem = document.createElement('input');
        this.elem.type = this._type;
        this.elem.className = this._className;
        this.elem.id = this._id;
        this.elem.name = this._name;
        this.elem.value = this._value;
        this.elem.placeholder = this._placeholder;
        this.elem.required = this._required;

        return this.elem;
    }

    addEventListener(event, listener) {
        this.elem.addEventListener(event, listener);
    }
}

class Select {
    constructor(name, id, className, additionalFunction, ...value) {
        this._name = name;
        this._id = id;
        this._value = value;
        this._className = className;
        this._additionalFunction = additionalFunction;
    }

    render() {
        const select = document.createElement('select');
        select.addEventListener('change', () => {
            this._additionalFunction(select)
        });
        select.id = this._id;
        select.setAttribute('name', this._name);
        select.classList.add(this._className);
        this._value.forEach((item) => {
            const option = document.createElement('option');
            option.setAttribute('value', item);
            option.innerText = item;
            select.append(option);
        });
        return select;
    }
}

class loginForm {
    constructor(id, action, onLogin) {
        this.id = id;
        this.action = action;
        this.onLogin = onLogin;
        this.form = null;
        this.button = null;
    }

    render() {
        this.form = document.createElement('form');
        this.form.className = 'form';
        this.form.id = this.id;
        this.form.action = this.action;
        this.email = new Input('email', 'input', 'inputEmail', 'Email', '', 'Введите email', 'true');
        this.password = new Input('password', 'input', 'inputPassword', 'Password', '', 'Введите пароль', 'true');
        this.button =  new Input('submit', 'submit', 'submit-login', '', 'Войти','', '');
        this.form.addEventListener('submit', this.onSubmit.bind(this));
        this.form.append(this.email.render());
        this.form.append(this.password.render());
        this.form.append(this.button.render());
        return this.form;
    }

    async onSubmit(event) {
        event.preventDefault();

        const inputEmail = document.getElementById('inputEmail');
        const inputPassword = document.getElementById('inputPassword');

        const data = {
            email: inputEmail.value,
            password: inputPassword.value
        };

        let response = await fetch('http://cards.danit.com.ua/login', {
            method: 'POST',
            body: JSON.stringify(data)
        });

        let result = await response.json();

        localStorage.setItem('token', result.token);
        this.onLogin();
    }
}

class CreateVisitForm {
    constructor(id, action) {
        this.id = id;
        this.action = action;
        this.form = null;
        this.button = null;

    }

    render() {

        this.form = document.createElement('form');
        this.form.className = 'form';
        this.form.id = this.id;
        this.form.action = this.action;
        this.div = document.createElement('div');
        this.div.className = 'div-in-form';
        this.doctor = new Select('doctor', 'selectDoctor', 'select-doctor', selectDoctor, 'Выберите доктора', 'Кардиолог', 'Терапевт', 'Стоматолог');
        this.button = new Input('button', 'submit', 'submit-create', '', 'Создать', '', '');

        this.form.append(this.div);
        this.divFields = document.createElement('div');
        this.divFields.className = 'div-in-form-inpt';
        this.div.prepend(this.doctor.render());
        this.div.append(this.divFields);

        this.divFields.after(this.button.render());

        return this.form
    }

    remover() {
        this.divFields.innerHTML = '';
    }

    submit() {
        this.btn = document.getElementById('btnCreateVisit');

        this.btn.addEventListener('click', async () => {
            this.inputs = Array.prototype.slice.call(document.querySelectorAll("input"), 2);
            this.select = document.querySelector('#selectDoctor');
            this.InputsObj = {};

            for (let item = 0; item < this.inputs.length-1; ++item) {
                this.InputsObj[this.inputs[item].name] = this.inputs[item].value;
                this.InputsObj[this.select.name] = this.select.value;
            }

            this.addCard = await fetch('http://cards.danit.com.ua/cards', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(this.InputsObj)
            });
            const thisMyModal = document.querySelector('.my-modal');
            thisMyModal.remove();
        });

    }

}

class CreateVisitCardiologist extends CreateVisitForm {
    constructor(fullName, pressure, bodyMassIndex, disease, age, ...args) {
        super(...args);
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.disease = disease;
        this.age = age;
    }

    render() {

        this.form = document.querySelector('.form');
        this.divFields = document.querySelector('.div-in-form-inpt');
        this.form.prepend(this.divFields);
        this.fullName = new Input('text', 'input', 'full-name-input', 'patientName', '', 'Введите ФИО', 'true');
        this.visitTarget = new Input('text', 'input', 'visit-target-input', 'visitTarget', '', 'Цель визита', 'true');
        this.description = new Input('text', 'input', 'description-input', 'description', '', 'Описание визита', 'true');
        this.pressure = new Input('text', 'input', 'pressureInpt', 'standartPressure', '', 'Нормальное давление', 'true');
        this.bodyMassIndex = new Input('text', 'input', 'massIndexInpt', 'BodyWeight', '', 'Индекс массы тела', 'true');
        this.disease = new Input('text', 'input', 'diseaseInpt', 'PreviousDiseases', '', 'Перенесенные болезни', 'true');
        this.age = new Input('number', 'input', 'ageInpt', 'age', '', 'Введите возраст', 'true');
        this.date = new Input('date', 'input', 'dateInpt', 'date', '', 'Дата визита', 'true');
        // this.urgency = new Select('Cрочность', 'selectDoctor', 'select-doctor', selectDoctor, 'Не срочно', 'Срочно', 'Очень срочно');

        this.divFields.innerHTML = '';

        this.divFields.append(this.fullName.render());
        this.divFields.append(this.date.render());
        // this.divFields.append(this.urgency.render());
        this.divFields.append(this.visitTarget.render());
        this.divFields.append(this.description.render());
        this.divFields.append(this.pressure.render());
        this.divFields.append(this.bodyMassIndex.render());
        this.divFields.append(this.disease.render());
        this.divFields.append(this.age.render());

        return this.form
        }
}

class CreateVisitDentist extends CreateVisitForm {
    constructor(fullName, ...args) {
        super(...args);
    }

    render() {

        this.form = document.querySelector('.form');
        this.divFields = document.querySelector('.div-in-form-inpt');
        this.form.prepend(this.divFields);
        // this.urgency = new Select('Cрочность', 'selectDoctor', 'select-doctor', selectDoctor, 'Не срочно', 'Срочно', 'Очень срочно');
        this.fullName = new Input('text', 'input', 'full-name-input', 'patientName', '', 'Введите ФИО', 'true');
        this.visitTarget = new Input('text', 'input', 'visit-target-input', 'visitTarget', '', 'Цель визита', 'true');
        this.description = new Input('text', 'input', 'description-input', 'description', '', 'Описание визита', 'true');
        this.lastVisit = new Input('date', 'input', 'lastInpt', 'lastVisit', '', 'Дата последнего визита', 'true');
        this.date = new Input('date', 'input', 'dateInpt', 'date', '', 'Дата визита', 'true');
        this.divFields.innerHTML = '';

        this.divFields.append(this.fullName.render());
        this.divFields.append(this.date.render());
        this.divFields.append(this.visitTarget.render());
        this.divFields.append(this.description.render());
        this.divFields.append(this.lastVisit.render());

        return this.form
    }
}

class CreateVisitTherapist extends CreateVisitForm {
    constructor(fullName, ...args) {
        super(...args);
    }

    render() {

        this.form = document.querySelector('.form');
        this.divFields = document.querySelector('.div-in-form-inpt');
        this.form.prepend(this.divFields);
        // this.urgency =  new Select('Cрочность', 'selectDoctor', 'select-doctor', selectDoctor, 'Не срочно', 'Срочно', 'Очень срочно');
        this.fullName = new Input('text', 'input', 'full-name-input', 'patientName', '', 'Введите ФИО', 'true');
        this.visitTarget = new Input('text', 'input', 'visit-target-input', 'visitTarget', '', 'Цель визита', 'true');
        this.description = new Input('text', 'input', 'description-input', 'description', '', 'Описание визита', 'true');
        this.date = new Input('date', 'input', 'dateInpt', 'date', '', 'Дата визита', 'true');

        this.divFields.innerHTML = '';

        this.divFields.append(this.fullName.render());
        this.divFields.append(this.date.render());
        this.divFields.append(this.visitTarget.render());
        this.divFields.append(this.description.render());

        return this.form
    }
}

class Card {
    constructor(fullName, visitTarget) {
        this.fullName = fullName;
        this.visitTarget = visitTarget;
        this.elem = null;
    }

    render() {

        this.elem = document.createElement('div');
        this.elem.className = 'visit-card';
        this._p = document.createElement('p');
        this._p.className = 'visit-field';

        let fullNameField = this._p.cloneNode(),
            visitTargetField = this._p.cloneNode();
        fullNameField.textContent = `ФИО: ${this.fullName}`;
        this.elem.append(fullNameField);
        this.elem.append(visitTargetField);

        return this.elem
    }

    async getCards() {
        this.cardBoard = document.querySelector('.card-board');
        this.response = await fetch('http://cards.danit.com.ua/cards', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        });
        this.visitCards = await this.response.json();

        if (this.visitCards.length === 0) {
            const emptyText = document.createElement('span');
            emptyText.className = 'empty-text';
            emptyText.innerText = 'No cards have been added';
            this.cardBoard.appendChild(emptyText);
        }
        
        this.visitCards.forEach(i => {
            
            this.cardVisit = document.createElement('div');
            this.cardVisit.className = 'visit-card';
            this.cardBoard.append(this.cardVisit);
            this._p = document.createElement('p');
            this._p.className = 'visit-field';

            let fullNameField = this._p.cloneNode(),
                visitTargetField = this._p.cloneNode(),
                doctorField = this._p.cloneNode(),
                dateField = this._p.cloneNode();
            doctorField.style.color = '#4a8996';
            doctorField.style.alignSelf = 'center';
            doctorField.style.fontWeight = 'bold';
            dateField.style.alignSelf = 'center';
            dateField.style.fontSize = 0.8 + 'em';
            fullNameField.textContent = `ФИО: ${i.patientName}`;
            dateField.textContent = i.date;
            visitTargetField.textContent = `Цель визита: ${i.visitTarget}`;
            doctorField.textContent = i.doctor;
            this.cardVisit.append(doctorField);
            this.cardVisit.append(dateField);
            this.cardVisit.append(fullNameField);
            this.cardVisit.append(visitTargetField);
        });
    }
}

function selectDoctor(select) {

    const selectedDoctor = select[select.selectedIndex].text;

    switch (selectedDoctor) {
        case 'Кардиолог':
            return CreateCardiologist.render();

        case 'Стоматолог':
            return CreateDentist.render();

        case 'Терапевт':
            return CreateTherapist.render();

        case 'Выберите доктора':
            return CreateForm.remover();

        default:
            return console.log('0')
    }
}

const CreateCardiologist  = new CreateVisitCardiologist('Аристотель Римский');
const CreateDentist = new CreateVisitDentist('Джеймс Хоуллет');
const CreateTherapist = new CreateVisitTherapist('Питтер Паркер');

const CreateForm = new  CreateVisitForm('create-form', '');
const modalCreate = new Modal('Create', CreateForm.render());
const GetCards = new Card();
const btnSign = document.getElementById('btnSign');
const btnCreateVisit = document.getElementById('btnCreateVisit');

btnCreateVisit.addEventListener('click', function() {
    modalCreate.render();
    CreateForm.submit();
});

btnSign.addEventListener('click', function() {
    const AuthorizationForm = new loginForm ('loginForm', '', onLogin);
    const modalWindow = new Modal('SignIn', AuthorizationForm.render());

    modalWindow.render();

    function onLogin() {
        const thisMyModal = document.querySelector('.my-modal');
        thisMyModal.remove();

        const btnLogin = document.getElementById('btnSign');
        btnLogin.style.display = 'none';

        const btnCreateVisit = document.getElementById('btnCreateVisit');

        btnCreateVisit.classList.remove('hidden');
        const cardBoard = document.createElement('div');

        cardBoard.className = 'card-board';
        document.body.appendChild(cardBoard);

        GetCards.getCards();
    }
});

 async function deleteCard (id) {
     await fetch(`http://cards.danit.com.ua/cards/${id}`, {
         method: 'Delete',
         headers: {
             Authorization: `Bearer ${localStorage.getItem('token')}`
         },
         body: JSON.stringify(this.InputsObj)
     })
 }
deleteCard();


